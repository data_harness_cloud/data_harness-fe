const projectConfig = {
  // baseUrl: import.meta.env.VITE_DATA_HARNESS_EXTERNAL_API,
  baseUrl: window.location.origin + '/api',
  // aiUrl: window.location.origin + '/v1',
  aiUrl: 'http://162.14.122.254:30391/v1',
  publicKey: import.meta.env.VITE_DATA_HARNESS_TOKEN,
  projectName: '驭数轻云数据平台',
}

export default projectConfig
